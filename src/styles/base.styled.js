// @ts-check
import { css } from 'styled-components';
import { flexColumn } from '../utils/style/mixins';

/**
 * Global styles
 * @param {ThemeType.StyledTheme} params
 */
export const BaseStyles = ({ font, colors, typography }) => {
  const { surface, text } = colors;
  const [blackHighContrast] = text.black;

  return css`
    *,
    :after,
    :before {
      box-sizing: border-box;
    }

    html,
    body {
      width: 100%;
      height: auto;
    }

    body {
      display: flex;
      flex-direction: column;
      background: ${surface.dark};
      font-family: ${font.primary};
      color: ${blackHighContrast};

      ${typography.body1};

      & > div {
        min-height: 100vh;
        ${flexColumn};
      }

      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      -webkit-touch-callout: none;
      -webkit-tap-highlight-color: transparent;
    }

    h1 {
      ${typography.h1};
    }

    h2 {
      ${typography.h2};
    }

    h3 {
      ${typography.h3};
    }

    h4 {
      ${typography.h4};
    }

    h5 {
      ${typography.h5};
    }

    h6 {
      ${typography.h6};
    }
  `;
};
