import { createGlobalStyle, css } from 'styled-components';
import { BaseStyles } from './base.styled';
import { ResetStyles } from './reset.styled';

// Base styles
export const AppGlobalStyles = createGlobalStyle(
  ({ theme }) => css`
    /** --- RESET STYLES --- **/
    ${ResetStyles(theme)}
    /** --- BASE STYLES --- **/
    ${BaseStyles(theme)}
  `
);
