// @ts-check
import React, { Suspense } from 'react';
import ErrorBoundary from 'react-error-boundary';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { ErrorFallback } from './components/ErrorFallback';
import { LoadingFallback } from './components/LoadingFallback';
import { IS_DEV } from './constants/settings';
import { AppLayout } from './layout/AppLayout';
import { routeNames } from './routes/route-names';
import { useStore } from './store';
import { styledTheme } from './utils/style/theme';
import { HomePage } from './routes/Home';

/**
 * Core project component
 * @type {React.FC}
 */
export const App = () => {
  const store = useStore({ logger: IS_DEV });

  return (
    <Router>
      <Provider store={store}>
        <ThemeProvider theme={styledTheme}>
          <ErrorBoundary FallbackComponent={ErrorFallback}>
            <Suspense fallback={<LoadingFallback />}>
              <AppLayout>
                <Switch>
                  <Route path={routeNames.home.url} exact component={HomePage} />
                  <Redirect to={routeNames.home.url} />
                </Switch>
              </AppLayout>
            </Suspense>
          </ErrorBoundary>
        </ThemeProvider>
      </Provider>
    </Router>
  );
};
