import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import { IS_DEV } from './constants/settings';

if (IS_DEV) {
  import('./mock-backend');
}

ReactDOM.render(<App />, document.getElementById('root'));
