declare global {
  module StoreType {
    interface Init {
      initialState?: object;
      logger?: boolean;
    }

    interface Reducers {
      [k: string]: object;
    }

    interface Epics {
      [k: string]: (props: any) => any;
    }

    interface Action {
      type: string;
      payload?: object;
      error?: boolean;
    }
  }
}

export {};
