import React from 'react';
import { FallbackProps } from 'react-error-boundary';

declare global {
  module ReactType {
    export interface Children {
      children: React.ReactNode;
    }

    export type GridComponent = (props: PropsT) => ReactType.Element;

    export type Element = React.ReactElement | null;

    // React.FunctionComponent<P>
    export type FC<P> = (props: P) => Element;

    export type FallbackPropsT = FallbackProps;
  }
}

export {};
