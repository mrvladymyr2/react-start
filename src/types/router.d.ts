declare global {
  module RoutesType {
    export interface Screens {
      home: 'home';
    }

    export type RouteSuffix = ':id';
    export type Screen = keyof Screens;

    interface RouteData {
      url: string;
      title: string;
    }

    type RouteNamesMap<T, J> = {
      [K in keyof T]: J;
    };

    export type RouteNames = RouteNamesMap<Screens, RouteData>;

    type getDynamicRouteByScreenId = (screen: Screen, id: number | string) => string;
  }
}

export {};
