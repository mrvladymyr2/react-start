declare global {
  module ThemeType {
    interface colorWeights {
      900?: string;
      800?: string;
      700?: string;
      600?: string;
      500?: string;
      400?: string;
      300?: string;
      200?: string;
      100?: string;
      50?: string;
      A700?: string;
      A400?: string;
      A200?: string;
      A100?: string;
    }

    export interface ColorPalette {
      primary: colorWeights;
      secondary: colorWeights;
    }

    export interface Surface {
      light: string;
      dark: string;
    }

    export interface Text {
      black: Array<string>;
      white: Array<string>;
    }

    export interface Colors {
      text: Text;
      surface: Surface;
      colorPalette: ColorPalette;
    }

    export interface Font {
      primary: string;
      regular: string;
      semibold: string;
    }

    export interface Sizes {
      xxxl: number;
      xxl: number;
      xl: number;
      lg: number;
      md: number;
      sm: number;
      xs: number;
    }

    export interface Breakpoints {
      xxl: number;
      xl: number;
      lg: number;
      md: number;
      sm: number;
      xs: number;
      zero: number;
    }

    export interface Typography {
      h1: object;
      h2: object;
      h3: object;
      h4: object;
      h5: object;
      h6: object;
      body1: object;
      body2: object;
      subtitle1: object;
      subtitle2: object;
      button: object;
      caption: object;
      overline: object;
    }

    export interface StyledTheme {
      font: Font;
      sizes: Sizes;
      colors: Colors;
      breakpoints: Breakpoints;
      typography: Typography;
    }
  }
}

export {};
