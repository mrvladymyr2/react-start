// @ts-check
import { colors } from './colors';

/**
 * Breakpoints declarations
 * @type {ThemeType.Breakpoints}
 */
const breakpoints = {
  xxl: 1600,
  xl: 1200,
  lg: 992,
  md: 768,
  sm: 576,
  xs: 575,
  zero: 0,
};

/**
 * Fonts declaration
 * @type {ThemeType.Font}
 */
const font = {
  primary: 'Open Sans, sans-serif',
  regular: '400',
  semibold: '600',
};

/**
 * Sizes declaration
 * @type {ThemeType.Sizes}
 */
const sizes = {
  xxxl: 64,
  xxl: 48,
  xl: 32,
  lg: 24,
  md: 16,
  sm: 14,
  xs: 12,
};

/**
 * Typography declarations
 * @type {ThemeType.Typography}
 */
const typography = {
  h1: {
    fontSize: '94.77px',
    fontWeight: font.regular,
    letterSpacing: '-1.5px',
  },
  h2: {
    fontSize: '58.7px',
    fontWeight: font.regular,
    letterSpacing: '-0.5px',
  },
  h3: {
    fontSize: '46.96px',
    fontWeight: font.regular,
    letterSpacing: '0px',
  },
  h4: {
    fontSize: '33.57px',
    fontWeight: font.regular,
    letterSpacing: '0.25px',
  },
  h5: {
    fontSize: '23.69px',
    fontWeight: font.regular,
    letterSpacing: '0px',
  },
  h6: {
    fontSize: '19.57px',
    fontWeight: font.semibold,
    letterSpacing: '0.25px',
  },
  body1: {
    fontSize: '15.8px',
    fontWeight: font.regular,
    letterSpacing: '0.5px',
  },
  body2: {
    fontSize: '13.82px',
    fontWeight: font.regular,
    letterSpacing: '0.25px',
  },
  subtitle1: {
    fontSize: '15.65px',
    fontWeight: font.semibold,
    letterSpacing: '0.15px',
  },
  subtitle2: {
    fontSize: '13.82px',
    fontWeight: font.regular,
    letterSpacing: '0.1px',
  },
  button: {
    fontSize: '13.7px',
    fontWeight: font.semibold,
    letterSpacing: '1.25px',
  },
  caption: {
    fontSize: '11.74px',
    fontWeight: font.semibold,
    letterSpacing: '0.4px',
  },
  overline: {
    fontSize: '11.74px',
    fontWeight: font.semibold,
    letterSpacing: '2px',
  },
};

/**
 * App styled theme
 * @type {ThemeType.StyledTheme}
 */
export const styledTheme = {
  font,
  sizes,
  colors,
  breakpoints,
  typography,
};
