// @ts-check
/**
 * Color palette
 * @type {ThemeType.ColorPalette}
 */
const colorPalette = {
  primary: {
    900: '#000000',
    800: '#262626',
    700: '#434343',
    600: '#555555',
    500: '#7B7B7B',
    400: '#9D9D9D',
    300: '#C4C4C4',
    200: '#D9D9D9',
    100: '#E9E9E9',
    50: '#F5F5F5',
  },
  secondary: {
    900: '#5D1049',
    800: '#821058',
    700: '#980F5F',
    600: '#AD0C67',
    500: '#BD086C',
    400: '#C33682',
    300: '#CA5998',
    200: '#D687B5',
    100: '#E5B6D2',
    50: '#F4E2ED',
    A700: '#E9028A',
    A400: '#FF0498',
    A200: '#FF37AD',
    A100: '#FF6AC2',
  },
};

/**
 * Surface
 * @type {ThemeType.Surface}
 */
const surface = {
  light: '#FFFFFF',
  dark: '#f5f5f5',
};

/**
 * Text colors
 * (get [0] - highContrast, [1] - midContrast, [2] - lowContrast)
 * @type {ThemeType.Text}
 */
const text = {
  black: ['rgba(0,0,0, .87)', 'rgba(0,0,0, .60)', 'rgba(0,0,0, .38)'],
  white: ['rgba(255,255,255, 100)', 'rgba(255,255,255, .60)', 'rgba(255,255,255, .38)'],
};

/**
 * App Colors settings
 * @type {ThemeType.Colors}
 */
export const colors = {
  text,
  surface,
  colorPalette,
};
