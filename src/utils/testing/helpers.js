/* eslint-disable */
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { useStore } from '../../store';
import { styledTheme } from '../style/theme';

// Selector for elements
export const sel = id => `[data-testid="${id}"]`;

// App context render
export function renderWithContext(component) {
  const store = useStore({ logger: false });

  return render(
    <Provider store={store}>
      <ThemeProvider theme={styledTheme}>{component}</ThemeProvider>
    </Provider>
  );
}

// App router render
export function renderWithRouter(
  ui,
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {}
) {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    history,
  }
}
