import axios from 'axios';
import { API_URL } from '../constants/settings';

// Request types
const GET = 'get';
const POST = 'post';
const PUT = 'put';
const DELETE = 'delete';

// Axios instance
export const axiosInstance = axios.create({
  baseURL: API_URL,
  timeout: 40000,
  headers: {},
});

const Request = (method, url, data) =>
  new Promise((resolve, reject) => {
    (() =>
      axiosInstance.request({
        url,
        method,
        ...(method === GET ? { params: data } : { data }),
      }))()
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.response);
      });
  });

export default {
  get: (endpoint, data) => Request(GET, endpoint, data),
  post: (endpoint, data) => Request(POST, endpoint, data),
  put: (endpoint, data) => Request(PUT, endpoint, data),
  del: (endpoint, data) => Request(DELETE, endpoint, data),
};
