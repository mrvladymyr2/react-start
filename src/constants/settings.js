export const IS_DEV = process.env.NODE_ENV === 'development';
export const API_URL = process.env.REACT_APP_API_URL;

// App API requests list
export const API = {
  EVAL_LIST: 'eval-list',
};
