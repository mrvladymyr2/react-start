// @ts-check
/**
 * @type {RoutesType.Screens}
 */
export const screens = {
  home: 'home',
};

/**
 * @type {RoutesType.RouteSuffix}
 */
export const DETAILS_SUFIX = ':id';

/**
 * @type {RoutesType.RouteNames}
 */
export const routeNames = {
  [screens.home]: { url: '/', title: 'Evaluate the cost' },
};

/**
 * @type {RoutesType.getDynamicRouteByScreenId}
 */
export function getDynamicRouteByScreenId(screen, id) {
  return routeNames[screen].url.replace(DETAILS_SUFIX, `${id}`);
}
