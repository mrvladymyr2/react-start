// @ts-check
import React from 'react';
import { useTitle } from 'react-use';
import { routeNames } from '../route-names';
import { Home } from './Home.styled';

/**
 * @type {React.FC}
 */
const HomePageComponent = () => {
  useTitle(routeNames.home.title);

  return <Home>{routeNames.home.title}</Home>;
};

export default HomePageComponent;
