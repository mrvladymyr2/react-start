import React from 'react';
import { renderWithContext } from '../../utils/testing/helpers';
import { HomePage } from '.';

jest.mock('../../components/Grid');

describe('<OrdersBrowser>', () => {
  const component = <HomePage />;

  it('renders properly', () => {
    renderWithContext(component);
  });

  it('match snapshot', () => {
    const { container } = renderWithContext(component);

    expect(container).toMatchSnapshot();
  });
});
