// @ts-check
// Redux libs
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';
// Dev tools
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
// Epics
import { epics } from './epics';
// Reducers
import { reducers } from './reducers';

/**
 * Use Dispatch Hook
 */
export function useDispatchMap(fn) {
  const dispatch = useDispatch();

  return useCallback(payload => dispatch(fn(payload)), [dispatch, fn]);
}

/**
 * Use Store Hook
 * @param {StoreType.Init} params
 */
export const useStore = ({ initialState = {}, logger = false }) => {
  const epicMiddleware = createEpicMiddleware();
  const loggerMiddleware = createLogger({ collapsed: true });

  const middlewareProps = [epicMiddleware, logger && loggerMiddleware].filter(Boolean);

  const store = createStore(
    // @ts-ignore
    combineReducers(reducers),
    initialState,
    composeWithDevTools(applyMiddleware(...middlewareProps))
  );

  epicMiddleware.run(combineEpics(...Object.values(epics)));

  return store;
};
