import React from 'react';
import { renderWithContext } from '../../utils/testing/helpers';
import AppLayout from './AppLayout';

describe('<AppLayout>', () => {
  const component = <AppLayout>Children inside</AppLayout>;

  it('renders properly', () => {
    renderWithContext(component);
  });

  it('match snapshot', () => {
    const { container } = renderWithContext(component);

    expect(container).toMatchSnapshot();
  });
});
