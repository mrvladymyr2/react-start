// @ts-check
import React from 'react';
import { AppGlobalStyles } from '../../styles/app.styled';
import { AppLayout } from './AppLayout.styled';

/**
 * App Layout component
 * @type {React.FC<ReactType.Children>}
 */
const AppLayoutComponent = ({ children }) => (
  <AppLayout>
    <AppGlobalStyles />
    {children}
  </AppLayout>
);

export default React.memo(AppLayoutComponent);
