import styled from 'styled-components';
import { flexColumn } from '../../utils/style/mixins';

/**
 * COMPONENT STYLES
 */
export const AppLayout = styled.div`
  ${flexColumn()};
`;
