import { render } from '@testing-library/react';
import React from 'react';
import LoadingFallback from './LoadingFallback';

describe('<LoadingFallback>', () => {
  const component = <LoadingFallback />;

  it('renders properly', () => {
    render(component);
  });

  it('match snapshot', () => {
    const { container } = render(component);

    expect(container).toMatchSnapshot();
  });
});
