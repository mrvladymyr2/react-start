// @ts-check
import React from 'react';

/**
 * @type {React.FC}
 */
const LoadingFallback = () => <div>Loading...</div>;

export default LoadingFallback;
