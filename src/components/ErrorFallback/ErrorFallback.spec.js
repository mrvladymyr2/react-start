import { render } from '@testing-library/react';
import React from 'react';
import ErrorFallback from './ErrorFallback';

describe('<ErrorFallback>', () => {
  const error = {
    name: 'render',
    message: 'Something happened',
  };
  const component = <ErrorFallback error={error} />;

  it('renders properly', () => {
    render(component);
  });

  it('match snapshot', () => {
    const { container } = render(component);

    expect(container).toMatchSnapshot();
  });
});
