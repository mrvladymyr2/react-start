// @ts-check
import React from 'react';

/**
 *
 * @type {React.FC<ReactType.FallbackPropsT>}
 */
const ErrorFallback = ({ error }) => ((
  <>
    <p>There was an error...</p>
    <pre style={{ maxWidth: 700 }}>{JSON.stringify(error, null, 2)}</pre>
  </>
));

export default ErrorFallback;
