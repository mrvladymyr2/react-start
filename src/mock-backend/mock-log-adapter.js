import MockAdapter from 'axios-mock-adapter';

class MockLogAdapter extends MockAdapter {
  static log({ config, status, data }) {
    console.groupCollapsed(`Request to: ${config.url}`);
    console.log('Status:', status);
    console.log('Response:', data);
    console.log('Params:', config);
    console.groupEnd();
  }

  plainLogger(status, data) {
    return function(config) {
      MockLogAdapter.log({ config, status, data });

      return [status, data];
    };
  }

  functionLogger(replyCallback) {
    return function(config) {
      const [status, data] = replyCallback(config);

      MockLogAdapter.log({ config, status, data });

      return [status, data];
    };
  }

  replyHandler(reply) {
    return (status, body) => {
      if (typeof status === 'function') {
        return reply(this.functionLogger(status));
      }

      return reply(this.plainLogger(status, body));
    };
  }

  onGet(matcher, body) {
    const box = MockAdapter.prototype.onGet.call(this, matcher, body);

    box.reply = this.replyHandler(box.reply);

    return box;
  }

  onPost(matcher, body) {
    const box = MockAdapter.prototype.onPost.call(this, matcher, body);

    box.reply = this.replyHandler(box.reply);

    return box;
  }
}

export default MockLogAdapter;
