import { axiosInstance } from '../utils/api-service';
import { getEvalList } from './get-eval-list';
import MockLogAdapter from './mock-log-adapter';

const mock = new MockLogAdapter(axiosInstance, { delayResponse: 800 });

getEvalList(mock);

mock.onAny().passThrough();
