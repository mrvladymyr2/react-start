import { API } from '../../constants/settings';
import evalData from './data-eval';

export const getEvalList = mock => {
  mock.onGet(API.EVAL_LIST).reply(200, evalData);
};
